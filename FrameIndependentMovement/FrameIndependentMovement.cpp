#include "stdafx.h"
#include "FrameIndependentMovement.h"
#include "SDL.h"
#include "SDL_image.h"
#include <stdio.h>
#include <map>

#define MIN_FRAMES            4
#define EXPECTED_FRAMES       50
#define MAX_FRAMES            300

#define GRAVITY 9.8f * (100.0f / (float)(EXPECTED_FRAMES*EXPECTED_FRAMES))

bool quit = false;
float xspeed, yspeed;
int sleepTime = 0;

typedef struct {
    float xpos;
    float ypos;
} State;

SDL_Window *sdlWindow = NULL;
SDL_Renderer *sdlRenderer = NULL;
SDL_Texture *sdlBackground = NULL;
SDL_Texture *sdlTexture = NULL;
std::map<SDL_Keycode, bool> keyPressed;

double getTimeInSec()
{
    static double coeff;
    static bool isInitialized;
    if (!isInitialized) {
        isInitialized = true;
        Uint64 freq = SDL_GetPerformanceFrequency();
        coeff = 1.0 / (double)freq;
    }
    Uint64 val = SDL_GetPerformanceCounter();
    return (double)val * coeff;
}

void drawTexture(SDL_Renderer *renderer, int x, int y, SDL_Texture *texture)
{
    SDL_Rect drawRect;
    drawRect.x = x;
    drawRect.y = y;
    SDL_QueryTexture(texture, NULL, NULL, &drawRect.w, &drawRect.h);
    SDL_RenderCopy(renderer, texture, NULL, &drawRect);
}

void updateEngine(State *curState)
{
    SDL_Event sdlEvent;

    //read input events from keyboard
    while (SDL_PollEvent(&sdlEvent)) {
        switch (sdlEvent.type) {
            case SDL_KEYDOWN:
                if (!keyPressed[sdlEvent.key.keysym.sym]) {
                    keyPressed[sdlEvent.key.keysym.sym] = true;
                }
                switch (sdlEvent.key.keysym.sym) {
                case SDLK_UP:
                    sleepTime += 5;
                    break;
                case SDLK_DOWN:
                    sleepTime -= 5;
                    if (sleepTime < 0) {
                        sleepTime = 0;
                    }
                    break;
                }
            break;
            case SDL_KEYUP:
                if (keyPressed[sdlEvent.key.keysym.sym]) {
                    keyPressed[sdlEvent.key.keysym.sym] = false;
                }
            break;
            case SDL_QUIT:
                quit = true;
            break;
        }
    }

    xspeed = 0;

    //read continuosly pressed keys
    if (keyPressed[SDLK_LEFT]) {
        xspeed = -3.6f;
    } else if (keyPressed[SDLK_RIGHT]) {
        xspeed = 3.6f;
    }

    //update the object position
    yspeed += GRAVITY;
    curState->xpos += xspeed;
    curState->ypos += yspeed;

    //do some a-posteriori collision detection
    if (curState->xpos > 800 - 60) {
        curState->xpos = 800 - 60;
    } else if (curState->xpos < 60) {
        curState->xpos = 60;
    }
    if (curState->ypos > 518 - 60) {
        curState->ypos = 518 - 60;
        yspeed = -16.8f;
    }
}

void renderScene(const State &drawState)
{
    //render the scene
    drawTexture(sdlRenderer, 0, 0, sdlBackground);
    drawTexture(sdlRenderer, (int)drawState.xpos - 60, (int)drawState.ypos - 60, sdlTexture);

    //simulate a slow rendering
    SDL_Delay(sleepTime);

    //present the screen
    SDL_RenderPresent(sdlRenderer);
}

#ifdef WIN32
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);
#else
int main(int argc, char* argv[]) {
#endif

    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_PNG);

    sdlWindow = SDL_CreateWindow("Frame Independent Movement", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
    if (!sdlWindow) {
        printf("Could not create window: %s\n", SDL_GetError());
        return 1;
    }

    sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!sdlRenderer) {
        printf("Could not create renderer: %s\n", SDL_GetError());
        return 1;
    }

    sdlBackground = IMG_LoadTexture(sdlRenderer, "gfx/background.pcx");
    sdlTexture = IMG_LoadTexture(sdlRenderer, "gfx/ball.png");

    State drawState, currentState, previousState;
    currentState.xpos = 200;
    currentState.ypos = 100;
    previousState = currentState;
    xspeed = 0;
    yspeed = 0;

    double t = 0, alpha, accumulator = 0;
    double deltaT = 1.0 / EXPECTED_FRAMES;
    double oldTime = getTimeInSec();

    while (!quit) {

        double currentTime = getTimeInSec();
        double frameDuration = currentTime - oldTime;

        if (frameDuration > 1.0 / MIN_FRAMES) {
            frameDuration = 1.0 / MIN_FRAMES;
        } else if (frameDuration < 1.0 / MAX_FRAMES) {
            SDL_Delay((int)(((1.0 / MAX_FRAMES) - frameDuration) * 1000.0));
            currentTime = getTimeInSec();
            frameDuration = currentTime - oldTime;
        }

        oldTime = currentTime;

        accumulator += frameDuration;
        while (accumulator >= deltaT) {
            previousState = currentState;
            updateEngine(&currentState);
            t += deltaT;
            accumulator -= deltaT;
        }

        alpha = accumulator / deltaT;
        drawState.xpos = currentState.xpos * (float)alpha + previousState.xpos * (float)(1.0 - alpha);
        drawState.ypos = currentState.ypos * (float)alpha + previousState.ypos * (float)(1.0 - alpha);

        renderScene(drawState);

        char windowTitle[256];
        windowTitle[0] = 0;
#if defined(_MSC_VER)
        sprintf_s(windowTitle, 256, "Frame Independent Movement [sleep: %d ms, fps : %d]", sleepTime, int(1.0 / frameDuration));
#else
        sprintf(windowTitle, "Frame Independent Movement [sleep: %d ms, fps : %d]", sleepTime, int(1.0 / frameDuration));
#endif
        SDL_SetWindowTitle(sdlWindow, windowTitle);
    }

    SDL_DestroyTexture(sdlTexture);
    SDL_DestroyTexture(sdlBackground);
    SDL_DestroyRenderer(sdlRenderer);
    SDL_DestroyWindow(sdlWindow);
    SDL_Quit();

    return 0;
}


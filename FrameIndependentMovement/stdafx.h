
#pragma once

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>

#ifdef WIN32
#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tchar.h>
#endif